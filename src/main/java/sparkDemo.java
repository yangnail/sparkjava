import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.col;

public class sparkDemo {


    public static void main(String[] args) {
        SparkSession spark = SparkSession
                .builder()
                .master("local")
                .appName("Java Spark SQL basic example")
                .config("spark.sql.warehouse.dir", "D:\\work\\bigdata\\code\\java\\spark-warehouse")
                .getOrCreate();

//D:\work\bigdata\code\java\people.json
        String input = "D:\\work\\bigdata\\code\\java\\people.json";
        Dataset<Row> df = spark.read().option("timestampFormat", "yyyy/MM/dd HH:mm:ss ZZ").json(input);
        df.show();
//        df.printSchema();
//        df.select("name").show();
//
//        df.select(col("ame"), col("age").plus(1)).show();
//        df.filter(col("age").gt(21)).show();
//        df.filter(col("age").gt(22)).show();
//        df.groupBy(col("age")).count().show();



    }

}
